# TTT-Server powered by Docker

## Requirements
Linux server with docker, docker-compose and git installed. Ports 27005 and 27015 need to be available. If you plan to set up your server at home, research how to set up portforwarding on your router.


## Getting started:
1. Download my project files. A folder will be created.
    > git clone https://gitlab.com/meffie96/ttt-docker.git
2. Change into the project folder
    > cd ttt-docker
3. Prepare and run the server. This will take some time, depending on your internet connection.
    > docker-compose up -d
4. Now you can check if everything works fine. Try to join your server in Gmod. If it works, you can now configure your server.


## Configure your server:
- Edit .env to host another workshop collection
    > nano .env
- Edit server.cfg to set TTT-rules. You can find all available settings [here](https://www.troubleinterroristtown.com/config/settings/).
    > nano server.cfg
- You need to restart your server after editing these values.
    > docker-compose down && docker-compose up -d


## Updating your server
If your server is out of date you need to restart your server.
> docker-compose restart