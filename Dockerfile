# Dockerfile for Garry's Mod server

# Use the official steamcmd image
FROM cm2network/steamcmd

# Update steam, install Garry's Mod Server and download Counter Strike Source files
RUN /home/steam/steamcmd/steamcmd.sh +force_install_dir "/home/steam/gmod" +login anonymous +app_update 4020 validate +quit
RUN /home/steam/steamcmd/steamcmd.sh +force_install_dir "/home/steam/css" +login anonymous +app_update 232330 validate +quit

# Copy mount config so GMod can find CSS files
COPY mount.cfg /home/steam/gmod/garrysmod/cfg/mount.cfg

# Entrypoint
ENTRYPOINT /home/steam/gmod/srcds_run -console -game garrysmod +maxplayers 16 +map ttt_minecraft_b5 +gamemode terrortown +host_workshop_collection 1697272655